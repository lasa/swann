#include <string>
#include <fstream>
#include <unordered_map>
#include <iostream>
#include <sstream>
#include <vector>
#include<bits/stdc++.h>


using namespace std; 

bool checkValidWord(string str, ifstream &inFile)
{
	//check str is a valid word from the file list provided

	vector<string> wordsVec;
	string word;
	int val;	
	
	while(getline(inFile, word))
	{
		val = word.compare(str);
		if(val > 0)
			return false;
		else if(val == 0)
			return true;		
	}

	return false;
}

void generateAnagram(string str, ifstream &inFile)
{
	string word;
	vector<string> words;
	bool validPhrase;

	//get all possible anagrams of the str
	sort(str.begin(), str.end());
	while( next_permutation( str.begin(), str.end() ))
	{
		cout << str << endl;
		validPhrase = true;
		istringstream input(str);    
    		while (input >> word)
		{
			//cout << word << endl;
        		validPhrase = validPhrase && (checkValidWord(word, inFile));
			if(!validPhrase)
			{
				break;
			}
		}

		if(validPhrase)
		{
			cout << str << endl;
		}
	}
}

int main()
{	
	ifstream inFile;
	vector<string> wordsVec;
	string strPhrase = "tac dog"; //school master";
	
	inFile.open("/usr/share/dict/words");
	if (inFile.fail()) 
	{
		cout << "cannot open file" << endl;
		return 1;
	}

	generateAnagram(strPhrase, inFile);
}

