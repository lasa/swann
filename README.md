# README #

Given a phrase with multiple words as input (eg. “school master”). 
This program generates all possible anagrams of the given phrase from the list of words provided (/usr/share/dict/words)

### Compiler Version ###

g++ --version
g++ (Ubuntu 7.5.0-3ubuntu1~18.04) 7.5.0

### Version ###

V1.0 - initial verion (timed work)
V2.0 - improvement of sorting strings before calculating hash value

### Usage ###

Compile: 
g++ -std=c++11 anagram.cpp -o anagram

Execute:
./anagram

### Output ###

anagram found: master : master
anagram found: school : school
anagram found: master : stream
anagram found: master : tamers

